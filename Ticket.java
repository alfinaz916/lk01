public class Ticket {

    private String namaPenumpang;
    private String asal;
    private String tujuan;

    public Ticket(String namaPenumpang){
        this.namaPenumpang = namaPenumpang;
    }

    public Ticket(String namaPenumpang, String asal, String tujuan) {
        this.namaPenumpang = namaPenumpang;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    public String getNamaPenumpang() {
        return namaPenumpang;
    }

    public String getasal() {
        return asal;
    }

    public String gettujuan() {
        return tujuan;
    }

    public void detailTiket() {
        if(asal == null) {
            System.out.println("Nama: " +this.namaPenumpang);
        }
        else {
            System.out.println("Nama: " +this.namaPenumpang);
            System.out.println("Asal: " +this.asal);
            System.out.println("Tujuan: " +this.tujuan);
            System.out.println("--------------------------");
        }
    }  
}