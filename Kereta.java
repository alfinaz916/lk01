import java.util.*;
public class Kereta {

    private String namaKereta;
    private int jumlahTiketTersedia;
    private ArrayList<Ticket> daftarTiket;

    // Default constructor untuk Kereta Komuter
    public Kereta() {
        this.namaKereta = "Komuter";
        this.jumlahTiketTersedia = 1000;
        this.daftarTiket = new ArrayList<Ticket>();
    }

    // Overload constructor untuk KAJJ
    public Kereta(String namaKereta, int jumlahTiketTersedia) {
        this.namaKereta = namaKereta;
        this.jumlahTiketTersedia = jumlahTiketTersedia;
        this.daftarTiket = new ArrayList<Ticket>();
    }

    // Overload method tambahTiket untuk KAJJ
    public void tambahTiket(String namaPenumpang, String asal, String tujuan) {
        if (jumlahTiketTersedia > 0) {
            Ticket tiket = new Ticket(namaPenumpang, asal, tujuan);
            daftarTiket.add(tiket);
            jumlahTiketTersedia--;
            System.out.println("Tiket berhasil dipesan." + " Sisa tiket tersedia : "+ jumlahTiketTersedia + " tiket.");
            System.out.println("================================================================================");
        } else {
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
            System.out.println("================================================================================");
        }
    }

    // Method tambahTiket untuk Kereta Komuter
    public void tambahTiket(String namaPenumpang) {
        if (jumlahTiketTersedia > 0) {
            jumlahTiketTersedia--;
            Ticket tiket = new Ticket(namaPenumpang);
            daftarTiket.add(tiket);
            if (jumlahTiketTersedia < 30) {
                System.out.println("Tiket berhasil dipesan");
                System.out.println("================================================================================");
            }
        } else {
            System.out.println("Maaf, tiket untuk " + namaKereta + " sudah habis terjual.");
        }
    }

    public void tampilkanTiket() {
        System.out.println("Daftar penumpang kereta api " + namaKereta + ":");
        System.out.println("------------------------------");
        for (Ticket ticket : daftarTiket) {  
            if (ticket == null) {
                break;
            } 
            else {
                ticket.detailTiket();
            }  
        }
    }

}